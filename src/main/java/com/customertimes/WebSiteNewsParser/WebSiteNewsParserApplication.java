package com.customertimes.WebSiteNewsParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSiteNewsParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebSiteNewsParserApplication.class, args);
	}

}
