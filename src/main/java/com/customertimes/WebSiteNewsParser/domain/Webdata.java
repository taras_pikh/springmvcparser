package com.customertimes.WebSiteNewsParser.domain;


import com.customertimes.WebSiteNewsParser.utils.Utils;

public class Webdata {

    private final String webHtml;

    public Webdata(final String webHtml) {
        this.webHtml = checkWebHtml(webHtml);
    }

    public String getWebData() {
        return checkWebHtml(this.webHtml);
    }

    private String checkWebHtml(final String htmlStr) {
        if (!isWebPage(htmlStr))
            throw new IllegalArgumentException();
        return htmlStr;
    }

    private boolean isWebPage(final String webData) {
        return Utils.checkWebPage(webData);
    }
}
