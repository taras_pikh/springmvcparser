package com.customertimes.WebSiteNewsParser.domain;

public class URL {

    public static final String OPENNET_URL_KEY = "opennet_news";
    public static final String MIRKNIG_URL_KEY = "mirknig_news";

    private final String urlString;
    private final String urlKey;

    public URL(final String urlString, final String urlKey) {
        this.urlString = checkUrl(urlString);
        this.urlKey = urlKey;
    }

    public String getValidUrl() {
        return checkUrl(this.urlString);
    }

    public String getValidUrlKey() {
        return this.urlKey;
    }

    private String checkUrl(final String url) {
        if (!(url == null || url == "" || url.startsWith("https://")))
            throw new IllegalArgumentException();
        return url;
    }
}
