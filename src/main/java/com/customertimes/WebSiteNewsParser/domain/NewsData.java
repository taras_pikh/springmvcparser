package com.customertimes.WebSiteNewsParser.domain;

import java.util.List;

public class NewsData {

    private final List<String> news;


    public NewsData(final List<String> news) {
        this.news = news;
    }

    public List<String> getTodayNews() {
        return this.news;
    }

//    private void checkNews(final List<String> news) {
//        if (news) {
//
//        }
//    }
}
