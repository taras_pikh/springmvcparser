package com.customertimes.WebSiteNewsParser.controller;

import com.customertimes.WebSiteNewsParser.domain.NewsData;
import com.customertimes.WebSiteNewsParser.domain.URL;
import com.customertimes.WebSiteNewsParser.domain.Webdata;
import com.customertimes.WebSiteNewsParser.service.newsGeneratorService.AbstractNewsService;
import com.customertimes.WebSiteNewsParser.service.downloadService.DownloadService;
import com.customertimes.WebSiteNewsParser.utils.SitesUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class SitesController {

    @Autowired
    private SitesUrl sitesUrl;

    @Autowired
    private DownloadService downloadService;

    @Autowired
    private AbstractNewsService abstractNewsService;

    @GetMapping("/")
    public String getMainPage() {
        return "main";
    }

    @GetMapping("/sitesList")
    public String getSitesList(Model model) {
        model.addAttribute("sitesLink", sitesUrl.getSitesURL());
        return "sites";
    }

    @GetMapping("/showTodayNews/{urlKey}")
    public String showTodayNews(@PathVariable String urlKey, Model model) {
        final URL siteUrl = new URL(sitesUrl.getLinkByKey(urlKey), urlKey);
        final Webdata siteData = downloadService.download(siteUrl);
        final NewsData todayNewsData = abstractNewsService.parseTodayNews(siteData, siteUrl);

        model.addAttribute("newsContent", todayNewsData.getTodayNews());
        return "siteTodayNews";
    }
}
