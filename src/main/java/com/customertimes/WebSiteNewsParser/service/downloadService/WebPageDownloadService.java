package com.customertimes.WebSiteNewsParser.service.downloadService;

import com.customertimes.WebSiteNewsParser.domain.URL;
import com.customertimes.WebSiteNewsParser.domain.Webdata;

public interface WebPageDownloadService {

    Webdata download(final URL url);
}
