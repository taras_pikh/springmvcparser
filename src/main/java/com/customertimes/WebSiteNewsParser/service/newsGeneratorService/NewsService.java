package com.customertimes.WebSiteNewsParser.service.newsGeneratorService;


import com.customertimes.WebSiteNewsParser.domain.NewsData;
import com.customertimes.WebSiteNewsParser.domain.URL;
import com.customertimes.WebSiteNewsParser.domain.Webdata;

public interface NewsService {

    NewsData parseTodayNews(final Webdata content, final URL url);
}
