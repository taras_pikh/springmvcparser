package com.customertimes.WebSiteNewsParser.service.newsGeneratorService;

import com.customertimes.WebSiteNewsParser.domain.NewsData;
import com.customertimes.WebSiteNewsParser.domain.URL;
import com.customertimes.WebSiteNewsParser.domain.Webdata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AbstractNewsService implements NewsService {

    @Autowired
    private OpenNetNewsServiceImpl openNetNewsService;

    @Autowired
    private MirknigNewsServiceImpl mirknigNewsService;

    @Override
    public NewsData parseTodayNews(final Webdata content, final URL url) {
        if (URL.OPENNET_URL_KEY.equals(url.getValidUrlKey())) {
            return openNetNewsService.parseTodayNews(content, url);
        }
        return mirknigNewsService.parseTodayNews(content, url);
    }
}
