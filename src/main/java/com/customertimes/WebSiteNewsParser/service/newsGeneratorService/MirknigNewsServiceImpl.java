package com.customertimes.WebSiteNewsParser.service.newsGeneratorService;

import com.customertimes.WebSiteNewsParser.domain.NewsData;
import com.customertimes.WebSiteNewsParser.domain.URL;
import com.customertimes.WebSiteNewsParser.domain.Webdata;
import com.customertimes.WebSiteNewsParser.utils.Utils;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class MirknigNewsServiceImpl implements NewsService {

    @Value("${date_pattern_contains}")
    private String DATE_PATTERN_CONTAINS;

    @Value("${opennet_date_format}")
    private String OPENNET_DATE_FORMAT;

    @Override
    public NewsData parseTodayNews(final Webdata siteContent, final URL url) {
        final List<String> contentList = Arrays.asList(siteContent.getWebData().split("</table>"));
        final List<String> news = new ArrayList<>();
        for(String content : contentList) {
            if(isTodayDay(content)) {
                final List<String> todayBlockElements = Arrays.asList(content.split("</tr>"));
                news.add(Jsoup.parse(todayBlockElements.get(1)).text());
            }
        }
        return new NewsData(news);
    }

    private boolean isTodayDay(final String content) {
        return (content.contains("Сегодня, ") || Utils.ifContainsDate(content, DATE_PATTERN_CONTAINS));
    }


}
