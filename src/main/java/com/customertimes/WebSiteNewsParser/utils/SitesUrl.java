package com.customertimes.WebSiteNewsParser.utils;

import com.customertimes.WebSiteNewsParser.domain.URL;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SitesUrl {

    @Value("${opennet_url}")
    private String OPENNET_URL;

    @Value("${mirking_url}")
    private String  MIRKNIG_URL;

    public List<URL> getSitesURL() {
        List<URL> urls = new ArrayList<>();
        urls.add(new URL(OPENNET_URL, URL.OPENNET_URL_KEY));
        urls.add(new URL(MIRKNIG_URL, URL.MIRKNIG_URL_KEY));
        return urls;
    }

    public String getLinkByKey(final String urlKey) {
        if (URL.OPENNET_URL_KEY.equals(urlKey)) {
            return OPENNET_URL;
        }
        return MIRKNIG_URL;
    }
}
