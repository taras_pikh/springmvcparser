package com.customertimes.WebSiteNewsParser.utils;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static final String HTML_TAG_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";

    public static boolean checkWebPage(final String webData) {
        return checkRegex(webData, HTML_TAG_PATTERN).find();
    }

    public static boolean ifContainsDate(final String webData, final String patternString) {
        return checkRegex(webData, patternString).matches();
    }

    private static Matcher checkRegex(final String webData, final String patternString) {
        final Pattern p = Pattern.compile(patternString);
        return p.matcher(webData);
    }

    public static String parseDate(final Date date, final String format) {
        final Format formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

}
